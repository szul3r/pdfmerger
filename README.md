# PDFmerger
**Python 3.8.0**<br />
This script combine all given PDF files in to one in order they will be given in parameters.<br />
If you want to give a name to merge file add as a last param -n flowed by the file name (without extension)<br />
example: <br />
`PDFmerger file1.pdf file2.pdf -n new_file`

type as param -h to get info