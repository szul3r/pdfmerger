import PyPDF2
import sys

final_file_name = None
arg = sys.argv[1:]
if '-n'in arg:
    *files, final_file_name = arg
    files.remove('-n')
    print(f'merging to file: {final_file_name}')
else:
    files = arg
if '-h' in arg:
    print('''
    This script combine all given PDF files in to one in order they will be given in parameters. 
    If you want to give a name to merge file add as a last param -n flowed by the file name (without extension)
    example:
    file1.pdf file2.pdf -n new_file''')
    exit()

del arg



def pdf_combainer(pdf_list, ffn = 'merged'):
    """
    Combain all given pdf files into one file
    :param pdf_list: list contains all files to combine
    :param ffn: name of new file. Default value 'merged'
    """
    merger = PyPDF2.PdfFileMerger()
    for pdf in pdf_list:
        print(f'merging file: {pdf}')
        merger.append(pdf)
    merger.write(f'{ffn}.pdf')

if final_file_name:
    pdf_combainer(files, final_file_name)
else:
    pdf_combainer(files)
